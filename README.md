# openATTIC build cache repository #

This repository is a cache of various JavaScript libraries (Node.js modules and Bower components)
that are required to build the [openATTIC](http://openattic.org/) WebUI.

It is managed by `utils/make_dist.py` included in the `openattic` git repository and gets updated
every time an openATTIC release build has been performed and the build configuration files
`webui/package.json` or `webui/bower.json` have been modified. This ensures that we can perform
repeatable builds of the openATTIC UI in case that any of these packages changes upstream.