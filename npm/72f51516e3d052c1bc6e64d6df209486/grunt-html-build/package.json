{
  "name": "grunt-html-build",
  "description": "Grunt HTML Builder - Appends scripts and styles, Removes debug parts, append html partials, Template options",
  "version": "0.3.1",
  "homepage": "https://github.com/spatools/grunt-html-build.git",
  "author": {
    "name": "SPA Tools",
    "url": "http://github.com/spatools/"
  },
  "repository": {
    "type": "git",
    "url": "git://github.com/spatools/grunt-html-build.git"
  },
  "bugs": {
    "url": "https://github.com/spatools/grunt-html-build/issues"
  },
  "devDependencies": {
    "grunt": "~0.4.1",
    "js-beautify": "~1.3.1"
  },
  "peerDependencies": {
    "grunt": "0.4.x",
    "js-beautify": "~1.3.1"
  },
  "dependencies": {
    "grunt": "0.4.x",
    "js-beautify": "~1.3.1"
  },
  "keywords": [
    "gruntplugin",
    "grunt",
    "htmlbuild",
    "html",
    "build",
    "htmlgen",
    "generate",
    "link"
  ],
  "readme": "# grunt-html-build [![NPM version](https://badge.fury.io/js/grunt-html-build.png)](http://badge.fury.io/js/grunt-html-build)\r\n\r\n[Grunt][grunt] HTML Builder - Appends scripts and styles, Removes debug parts, append html partials, Template options\r\n\r\n## Getting Started\r\n\r\nInstall this grunt plugin next to your project's gruntfile with: `npm install grunt-html-build --save-dev`\r\n\r\nThen add this line to your project's `Gruntfile.js` :\r\n\r\n```javascript\r\ngrunt.loadNpmTasks('grunt-html-build');\r\n```\r\n\r\nThen specify your config: ([more informations][doc-options])\r\n\r\n```javascript\r\ngrunt.initConfig({\r\n    fixturesPath: \"fixtures\",\r\n\r\n    htmlbuild: {\r\n        dist: {\r\n            src: 'index.html',\r\n            dest: 'samples/',\r\n            options: {\r\n                beautify: true,\r\n                prefix: '//some-cdn',\r\n\t\t\t\trelative: true,\r\n                scripts: {\r\n                    bundle: [\r\n                        '<%= fixturesPath %>/scripts/*.js',\r\n                        '!**/main.js',\r\n                    ],\r\n                    main: '<%= fixturesPath %>/scripts/main.js'\r\n                },\r\n                styles: {\r\n                    bundle: [\r\n                        '<%= fixturesPath %>/css/libs.css',\r\n                        '<%= fixturesPath %>/css/dev.css'\r\n                    ],\r\n                    test: '<%= fixturesPath %>/css/inline.css'\r\n                },\r\n                sections: {\r\n                    views: '<%= fixturesPath %>/views/**/*.html',\r\n                    templates: '<%= fixturesPath %>/templates/**/*.html',\r\n\t\t\t\t\tlayout: {\r\n\t\t\t\t\t\theader: '<%= fixturesPath %>/layout/header.html',\r\n\t\t\t\t\t\tfooter: '<%= fixturesPath %>/layout/footer.html'\r\n\t\t\t\t\t}\r\n                },\r\n                data: {\r\n\t\t\t\t\t// Data to pass to templates\r\n                    version: \"0.1.0\",\r\n                    title: \"test\",\r\n                },\r\n            }\r\n        }\r\n    }\r\n});\r\n```\r\n\r\nUsing the configuration above, consider the following example html to see it in action:\r\n\r\n```html\r\n<html>\r\n<head>\r\n    <title>grunt-html-build - Test Page</title>\r\n    <!-- build:style bundle -->\r\n    <link rel=\"stylesheet\" type=\"text/css\" href=\"/path/to/css/dev.css\" />\r\n    <!-- /build -->\r\n    <!-- build:style inline test -->\r\n    <link rel=\"stylesheet\" type=\"text/css\" href=\"/path/to/css/dev-inline.css\" />\r\n    <!-- /build -->\r\n</head>\r\n<body id=\"landing-page\">\r\n\t<!-- build:section layout.header -->\r\n\t<!-- /build -->\r\n\r\n    <!-- build:section views -->\r\n    <!-- /build -->\r\n\r\n\t<!-- build:section layout.footer -->\r\n\t<!-- /build -->\r\n\r\n    <!-- build:remove -->\r\n    <script type=\"text/javascript\" src=\"/path/to/js/only-dev.js\"></script>\r\n    <!-- /build -->\r\n\r\n    <!-- build:script bundle -->\r\n    <script type=\"text/javascript\" src=\"/path/to/js/libs/jquery.js\"></script>\r\n    <script type=\"text/javascript\" src=\"/path/to/js/libs/knockout.js\"></script>\r\n    <script type=\"text/javascript\" src=\"/path/to/js/libs/underscore.js\"></script>\r\n    <script type=\"text/javascript\" src=\"/path/to/js/app/module1.js\"></script>\r\n    <script type=\"text/javascript\" src=\"/path/to/js/app/module2.js\"></script>\r\n    <!-- /build -->\r\n    <!-- build:process -->\r\n    <script type=\"text/javascript\">\r\n        var version = \"<%= version %>\",\r\n            title = \"<%= title %>\";\r\n    </script>\r\n    <!-- /build -->\r\n    <!-- build:script inline main -->\r\n    <script type=\"text/javascript\">\r\n        main();\r\n    </script>\r\n    <!-- /build -->\r\n\r\n    <!-- build:section optional test -->\r\n    <!-- /build -->\r\n</body>\r\n</html>\r\n```\r\n\r\nAfter running the grunt task it will be stored on the dist folder as\r\n\r\n```html\r\n<html>\r\n    <head>\r\n        <title>grunt-html-build - Test Page</title>\r\n        <link type=\"text/css\" rel=\"stylesheet\" href=\"../fixtures/css/libs.css\" />\r\n        <link type=\"text/css\" rel=\"stylesheet\" href=\"../fixtures/css/dev.css\" />\r\n        <style>\r\n            .this-is-inline {\r\n                font-weight: bold;\r\n            }\r\n        </style>\r\n    </head>\r\n    <body id=\"landing-page\">\r\n\t\t<header>...</header>\r\n        <div id=\"view1\">...</div>\r\n        <div id=\"view2\">...</div>\r\n        <div id=\"view3\">...</div>\r\n\t\t<footer>...</footer>\r\n        <script type=\"text/javascript\" src=\"../fixtures/scripts/app.js\"></script>\r\n        <script type=\"text/javascript\" src=\"../fixtures/scripts/libs.js\"></script>\r\n        <script type=\"text/javascript\">\r\n            var version = \"0.1.0\",\r\n                title = \"test\";\r\n        </script>\r\n        <script type=\"text/javascript\">\r\n            productionMain();\r\n        </script>\r\n    </body>\r\n</html>\r\n```\r\n\r\nThere 5 types of processors:\r\n\r\n * [script][doc-scripts-styles]\r\n\t* append script reference from configuration to dest file.\r\n * [style][doc-scripts-styles]\r\n\t* append style reference from configuration to dest file.\r\n * [section][doc-sections]\r\n\t* append partials from configuration to dest file.\r\n * [process][doc-process]\r\n\t* process grunt template on the block.\r\n * [remove][doc-remove]\r\n\t* it will erase the whole block.\r\n\r\n[grunt]: https://github.com/gruntjs/grunt\r\n[doc-options]: https://github.com/spatools/grunt-html-build/wiki/Task-Options\r\n[doc-scripts-styles]: https://github.com/spatools/grunt-html-build/wiki/Linking-Scripts-and-Styles\r\n[doc-sections]: https://github.com/spatools/grunt-html-build/wiki/Creating-HTML-Sections\r\n[doc-process]: https://github.com/spatools/grunt-html-build/wiki/Using-HTML-as-Template\r\n[doc-remove]: https://github.com/spatools/grunt-html-build/wiki/Removing-parts\r\n[doc-reuse]: https://github.com/spatools/grunt-html-build/wiki/Creating-reusable-HTML-Layout-Template\r\n\r\n## Release History\r\n* 0.1.0 Initial Release\r\n* 0.1.1 Cleaning, adding optional tags, using js-beautify\r\n* 0.1.2 Adding expand options to tags paths and write docs\r\n* 0.1.3 Fixing nodejs dependencies\r\n* 0.1.4 Fixing nodejs dependencies\r\n* 0.1.5 Optimize src loop / Fix js-beautify options\r\n* 0.1.6 Allow build tag customization\r\n* 0.2.0 \r\n\t* Fix and optimisation\r\n\t* Allow replacing src file by built file\r\n\t* Allow filename in dest path\r\n\t* Allow prefixing src files\r\n* 0.2.1 Allow non relative file names + per file tag parameter\r\n* 0.2.2 Fix issue in options.relative\r\n* 0.3.0\r\n\t* Fix issue when building multiple html files using custom file globbing\r\n\t* Allow sub parameters in all options paths\r\n* 0.3.1\r\n\t* Fix issue when using prefix on Windows environment\r\n",
  "readmeFilename": "README.md",
  "_id": "grunt-html-build@0.3.1",
  "_from": "grunt-html-build@0.3.1"
}
