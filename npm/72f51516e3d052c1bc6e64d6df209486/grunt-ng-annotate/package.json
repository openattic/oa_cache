{
  "name": "grunt-ng-annotate",
  "version": "1.0.1",
  "description": "Add, remove and rebuild AngularJS dependency injection annotations.",
  "homepage": "https://github.com/mzgol/grunt-ng-annotate",
  "author": {
    "name": "Michał Gołębiowski",
    "email": "m.goleb@gmail.com"
  },
  "keywords": [
    "angular",
    "angularjs",
    "annotate",
    "annotation",
    "annotations",
    "di",
    "dependency",
    "injection",
    "minify",
    "grunt",
    "gruntplugin",
    "non-intrusive",
    "transformation"
  ],
  "repository": {
    "type": "git",
    "url": "https://github.com/mzgol/grunt-ng-annotate.git"
  },
  "bugs": {
    "url": "https://github.com/mzgol/grunt-ng-annotate/issues"
  },
  "license": "MIT",
  "files": [
    "tasks"
  ],
  "dependencies": {
    "lodash.clonedeep": "^3.0.1",
    "ng-annotate": "^1.0.0"
  },
  "devDependencies": {
    "babel-core": "5.4.7",
    "convert-source-map": "1.1.1",
    "expect.js": "0.3.1",
    "grunt": "0.4.5",
    "grunt-babel": "5.0.0",
    "grunt-cli": "0.1.13",
    "grunt-contrib-clean": "0.6.0",
    "grunt-contrib-copy": "0.8.0",
    "grunt-eslint": "13.0.0",
    "grunt-jscs": "1.8.0",
    "grunt-mocha-test": "0.12.7",
    "jscs-trailing-comma": "0.4.2",
    "load-grunt-tasks": "3.2.0",
    "mocha": "2.2.5",
    "source-map": "0.4.2",
    "time-grunt": "1.2.1"
  },
  "peerDependencies": {
    "grunt": "~0.4.5"
  },
  "scripts": {
    "test": "grunt"
  },
  "engines": {
    "node": ">= 0.10.0"
  },
  "readme": "# grunt-ng-annotate\n> Add, remove and rebuild AngularJS dependency injection annotations. Based on [ng-annotate](https://www.npmjs.org/package/ng-annotate).\n\n[![Build Status](https://travis-ci.org/mzgol/grunt-ng-annotate.svg?branch=master)](https://travis-ci.org/mzgol/grunt-ng-annotate)\n[![Build status](https://ci.appveyor.com/api/projects/status/rr3i854ic8rb47i5/branch/master?svg=true)](https://ci.appveyor.com/project/mzgol/grunt-ng-annotate/branch/master)\n[![Built with Grunt](https://cdn.gruntjs.com/builtwith.png)](http://gruntjs.com/)\n\n## Getting Started\nThis plugin requires Grunt.\n\nIf you haven't used [Grunt](http://gruntjs.com/) before, be sure to check out the [Getting Started](http://gruntjs.com/getting-started) guide, as it explains how to create a [Gruntfile](http://gruntjs.com/sample-gruntfile) as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:\n\n```shell\nnpm install grunt-ng-annotate --save-dev\n```\n\nOnce the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:\n\n```js\ngrunt.loadNpmTasks('grunt-ng-annotate');\n```\n\n## Overview\n\nThis project defines the `ngAnnotate` task. In your project's Gruntfile, add a section named `ngAnnotate` to the data object passed into `grunt.initConfig()`.\n\n```js\ngrunt.initConfig({\n    ngAnnotate: {\n        options: {\n            // Task-specific options go here.\n        },\n        your_target: {\n            // Target-specific file lists and/or options go here.\n        },\n    },\n})\n```\n\n## Options\n\nThe `ngAnnotate` task accepts a couple of options:\n\n### add\n\nTells if ngAnnotate should add annotations.\n\nType: `boolean`\n\nDefault: `true`\n\n### remove\n\nTells if ngAnnotate should remove annotations.\n\nType: `boolean`\n\nDefault: `false`\n\nNote that both `add` and `remove` options can be set to true; in such a case `ngAnnotate` first removes\nannotations and then re-adds them (it can be used to check if annotations were provided correctly).\n\n### regexp\n\nIf provided, only strings matched by the regexp are interpreted as module names. You can provide both a regular expression and a string representing one. See README of ng-annotate for further details: https://npmjs.org/package/ng-annotate\n\nType: `regexp`\n\nDefault: none\n\n### singleQuotes\n\nSwitches the quote type for strings in the annotations array to single ones; e.g. `'$scope'` instead of `\"$scope\"`.\n\nType: `boolean`\n\nDefault: `false`\n\n### separator\n\nConcatenated files will be joined on this string. \n\nType: `string`\n\nDefault: `grunt.util.linefeed`\n\nIf you're post-processing concatenated JavaScript files with a minifier, you may need to use a semicolon ';' as the separator.\n\n### sourceMap\n\nEnables source map generation.\n\nType: `boolean` or `string`\n\nDefault: `false`\n\nIf set to a string, the string points to a file where to save the source map. If set to `true`, an inline source map will be used.\n\n### ngAnnotateOptions\n\nIf ngAnnotate supports a new option that is not directly supported via this Grunt task yet, you can pass it here. These options gets merged with the above specific to ngAnnotate. Options passed here have lower precedence to the direct ones described above.\n\nType: `object`\n\nDefault: `{}`\n\n\n## Usage Examples\n\n```js\ngrunt.initConfig({\n    ngAnnotate: {\n        options: {\n            singleQuotes: true,\n        },\n        app1: {\n            files: {\n                'a.js': ['a.js'],\n                'c.js': ['b.js'],\n                'f.js': ['d.js', 'e.js'],\n            },\n        },\n        app2: {\n            files: [\n                {\n                    expand: true,\n                    src: ['f.js'],\n                    ext: '.annotated.js', // Dest filepaths will have this extension.\n                    extDot: 'last',       // Extensions in filenames begin after the last dot\n                },\n            ],\n        },\n        app3: {\n            files: [\n                {\n                    expand: true,\n                    src: ['g.js'],\n                    rename: function (dest, src) { return src + '-annotated'; },\n                },\n            ],\n        },\n    },\n});\n\ngrunt.loadNpmTasks('grunt-ng-annotate');\n```\n\nAfter executing `grunt ngAnnotate`, you'll get file `a.js` annotated and saved under the same name, file `b.js`\nannotated and saved as `c.js` and files `d.js` and `e.js` concatenated, annotated and saved as `f.js`. Annotations\nwill be saved using single quotes.\n\nAn annotated version of the `f.js` file will be saved as `f.annotated.js` and an annotated version of the `g.js` file will be saved as `g.js-annotated`. \n\n## Contributing\nIn lieu of a formal styleguide, take care to maintain the existing coding style. Add unit tests for any new or changed\nfunctionality. Lint and test your code using [Grunt](http://gruntjs.com/).\n\n## License\nCopyright (c) 2014 Michał Gołębiowski. Licensed under the MIT license.\n",
  "readmeFilename": "README.md",
  "_id": "grunt-ng-annotate@1.0.1",
  "dist": {
    "shasum": "78ed87435e45ecda9b5825e9d6dfc82233cae65d"
  },
  "_from": "grunt-ng-annotate@1.0.1",
  "_resolved": "https://registry.npmjs.org/grunt-ng-annotate/-/grunt-ng-annotate-1.0.1.tgz"
}
